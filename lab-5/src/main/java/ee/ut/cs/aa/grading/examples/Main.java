package ee.ut.cs.aa.grading.examples;

import ee.ut.cs.aa.grading.tree.util.Drawer;
import ee.ut.cs.aa.grading.tree.util.DrawerImpl;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Drawer drawer = new DrawerImpl();
        BinarySearchTree<Integer, Integer> bst = new AvlTree<>();

        // Add some random values.
        for (int i = 0; i < 10; i++) {
            Integer val = i;
            bst.add(val, val);
            System.out.println("Added " + val);
            drawer.draw(bst.toBinaryTreeVertex());
            Thread.sleep(2000);
        }

        // Draw tree
        //drawer.draw(bst.toBinaryTreeVertex());

        // Try to remove some values.
        for (int i = 0; i < 20; i++) {
            while (bst.remove(i) != null) {
                System.out.println("Removed " + i);
                try {
                    drawer.draw(bst.toBinaryTreeVertex());
                } catch (NullPointerException e) {
                    System.out.println("No elements left in tree");
                }
                Thread.sleep(2000);
            }
        }
    }
}
