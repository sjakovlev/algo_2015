package ee.ut.cs.aa.grading.examples;

import ee.ut.cs.aa.grading.AVL.TreeMap;
import ee.ut.cs.aa.grading.tree.BinaryTreeVertex;

public class BinarySearchTree<K extends Comparable<K>, V> implements TreeMap<K, V> {

    protected BinaryTreeVertex<Element<K, V>> root;

    @Override
    public void add(K key, V value) {
        Element<K, V> newElement = new Element<>(key, value);

        if (root == null) {
            // If tree is empty, create a new root with given element.
            root = new BinaryTreeVertex<>(newElement);
        } else {
            internalAdd(newElement, root);
        }
    }

    /**
     * Add element to given binary search tree.
     *
     * @param newElement element that will be added
     * @param currentVertex root of the subtree to which the element will be added, cannot be null
     */
    private void internalAdd(Element<K, V> newElement, BinaryTreeVertex<Element<K, V>> currentVertex) {
        if (newElement.getKey().compareTo(currentVertex.getData().getKey()) < 0) {
            // Key of the added element is smaller, add new element to the left subtree
            if (currentVertex.getLeftVertex() == null) {
                currentVertex.setLeftVertex(new BinaryTreeVertex<>(newElement));

                balanceFromVertexToRoot(currentVertex.getParent()); // NOTE: invoke hook for AVL subclasses
            } else {
                internalAdd(newElement, currentVertex.getLeftVertex());
            }
        } else {
            // Key of the added element is greater or equal to the key of the current vertex,
            // add new element to the right subtree.
            if (currentVertex.getRightVertex() == null) {
                currentVertex.setRightVertex(new BinaryTreeVertex<>(newElement));

                balanceFromVertexToRoot(currentVertex.getParent()); // NOTE: invoke hook for AVL subclasses
            } else {
                internalAdd(newElement, currentVertex.getRightVertex());
            }
        }
    }

    @Override
    public V get(K key) {
        BinaryTreeVertex<Element<K, V>> foundVertex = getVertex(key);
        return foundVertex != null ? foundVertex.getData().getValue() : null;
    }

    /**
     * Find vertex with a matching key.
     *
     * @param key key that must be found
     * @return found vertex or null if no vertex with such key could be found
     */
    private BinaryTreeVertex<Element<K, V>> getVertex(K key) {
        BinaryTreeVertex<Element<K, V>> currentVertex = root;

        while (currentVertex != null) {
            K currentKey = currentVertex.getData().getKey();
            if (key.equals(currentKey)) {
                return currentVertex;
            }

            if (key.compareTo(currentKey) < 0) {
                currentVertex = currentVertex.getLeftVertex();
            } else {
                currentVertex = currentVertex.getRightVertex();
            }
        }

        return null;
    }

    @Override
    public V remove(K key) {
        // Find a vertex that has to be removed.
        BinaryTreeVertex<Element<K, V>> vertex = getVertex(key);

        if (vertex == null) {
            // If not found, just return null.
            return null;
        }

        // Remove found vertex and return its value.
        removeVertex(vertex);
        return vertex.getData().getValue();
    }

    /**
     * Remove given vertex from the binary search tree.
     *
     * @param vertex that has to be removed
     */
    private void removeVertex(BinaryTreeVertex<Element<K, V>> vertex) {
        // Parent of the vertex we have to remove.
        // Null if we want to remove root vertex.
        BinaryTreeVertex<Element<K, V>> parent = vertex.getParent();

        if (vertex.getLeftVertex() == null && vertex.getRightVertex() == null) {
            // Case 1 - removing vertex with no children.
            if (parent == null) {
                root = null;
            } else if (parent.getLeftVertex() == vertex) {
                parent.setLeftVertex(null);
            } else {
                parent.setRightVertex(null);
            }

            balanceFromVertexToRoot(parent); // NOTE: invoke hook for AVL subclasses
        } else if (vertex.getRightVertex() == null) {
            // Case 2.1 - removing vertex that has only left child.
            if (parent == null) {
                root = vertex.getLeftVertex();
                // Update parent reference!
                root.setParent(null);
            } else if (parent.getLeftVertex() == vertex) {
                parent.setLeftVertex(vertex.getLeftVertex());
            } else {
                parent.setRightVertex(vertex.getLeftVertex());
            }

            balanceFromVertexToRoot(parent); // NOTE: invoke hook for AVL subclasses
        } else if (vertex.getLeftVertex() == null) {
            // Case 2.2 - removing vertex that has only right child.
            if (parent == null) {
                root = vertex.getRightVertex();
                // Update parent reference!
                root.setParent(null);
            } else if (parent.getLeftVertex() == vertex) {
                parent.setLeftVertex(vertex.getRightVertex());
            } else {
                parent.setRightVertex(vertex.getRightVertex());
            }

            balanceFromVertexToRoot(parent); // NOTE: invoke hook for AVL subclasses
        } else {
            // Case 3 - removing vertex that has both children.

            // Find replacement and remove it from the binary search tree.
            // Replacement is a vertex that has the smallest key from the right subtree.
            // (Replacement can also be a vertex that has the greatest key from the left subtree.)
            // We know that replacement will have no left child (it will be leftmost element of a subtree).
            BinaryTreeVertex<Element<K, V>> replacement = getMinimumVertex(vertex.getRightVertex());
            removeVertex(replacement);

            // Replace removed vertex with the replacement.
            if (parent == null) {
                root = replacement;
                // Update parent reference!
                root.setParent(null);
            } else if (parent.getLeftVertex() == vertex) {
                parent.setLeftVertex(replacement);
            } else {
                parent.setRightVertex(replacement);
            }

            // Change children of the replacement.
            replacement.setLeftVertex(vertex.getLeftVertex());
            replacement.setRightVertex(vertex.getRightVertex());
        }
    }

    /**
     * Balance vertices starting from the given one until the root of the tree.
     *
     * This method is needed for {@link AvlTree} subclass to painlessly implement tree balancing.
     * Although usually we should avoid adding functionality needed for a subclass into its superclass, in this case
     * this significantly simplifies solution and allows to avoid code duplication (DRY).
     *
     * @param vertex balancing should be started at this vertex (may be null)
     */
    @SuppressWarnings("UnusedParameters")
    protected void balanceFromVertexToRoot(BinaryTreeVertex<Element<K, V>> vertex) {
        // Intentionally left empty for subclasses to override
    }

    /**
     * Find vertex with the smallest key from the given binary search tree.
     *
     * @param currentVertex root of the tree that contains vertex that will be returned
     * @return vertex with the smallest key
     */
    private BinaryTreeVertex<Element<K, V>> getMinimumVertex(BinaryTreeVertex<Element<K, V>> currentVertex) {
        while (currentVertex.getLeftVertex() != null) {
            currentVertex = currentVertex.getLeftVertex();
        }
        return currentVertex;
    }

    /*
     * The following methods are required to inspect the tree in automatic tests.
     */

    @Override
    public String toParenNotation() {
        // TODO: Implement this method!
        return null;
    }

    @Override
    public TreeMap<Integer, String> fromParenNotation(String tree) {
        // TODO: Implement this method!
        return null;
    }

    /*
     * The following methods are needed to use Drawer.
     */

    @Override
    public BinaryTreeVertex<K> toBinaryTreeVertex() {
        return toBinaryTreeVertex(root);
    }

    /**
     * Make a copy of given tree that contains only keys from the original tree.
     *
     * @param currentVertex tree that will be copied
     * @return copy of the given tree that contains only keys
     */
    private BinaryTreeVertex<K> toBinaryTreeVertex(
            BinaryTreeVertex<Element<K, V>> currentVertex) {
        if (currentVertex == null) {
            return null;
        }

        BinaryTreeVertex<K> convertedVertex = new BinaryTreeVertex<>(currentVertex.getData().getKey());
        convertedVertex.setLeftVertex(toBinaryTreeVertex(currentVertex.getLeftVertex()));
        convertedVertex.setRightVertex(toBinaryTreeVertex(currentVertex.getRightVertex()));

        return convertedVertex;
    }
}
