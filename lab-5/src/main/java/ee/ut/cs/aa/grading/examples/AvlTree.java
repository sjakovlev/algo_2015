package ee.ut.cs.aa.grading.examples;

import ee.ut.cs.aa.grading.AVL.TreeMap;
import ee.ut.cs.aa.grading.tree.BinaryTreeVertex;

public class AvlTree<K extends Comparable<K>, V> extends BinarySearchTree<K, V> implements TreeMap<K, V> {

    private int getTreeHeight(BinaryTreeVertex currentVertex) {
        if (currentVertex == null) {
            return -1;
        }

        int leftHeight = getTreeHeight(currentVertex.getLeftVertex());
        int rightHeight = getTreeHeight(currentVertex.getRightVertex());

        return Math.max(leftHeight, rightHeight) + 1;
    }

    private void rotateLeft(BinaryTreeVertex<Element<K, V>> currentVertex) {
        // We can assume that the right child does exist.
        BinaryTreeVertex<Element<K, V>> rightChild = currentVertex.getRightVertex();

        // If parent is null, currentVertex is the root of the entire tree.
        BinaryTreeVertex<Element<K, V>> rotationParent = currentVertex.getParent();

        if (rotationParent == null) {
            root = rightChild;
            rightChild.setParent(null);
        } else if (rotationParent.getLeftVertex() == currentVertex) {
            rotationParent.setLeftVertex(rightChild);
        } else {
            rotationParent.setRightVertex(rightChild);
        }

        currentVertex.setRightVertex(rightChild.getLeftVertex());
        rightChild.setLeftVertex(currentVertex);
    }

    private void rotateRight(BinaryTreeVertex<Element<K, V>> currentVertex) {
        // We can assume that the left child does exist.
        BinaryTreeVertex<Element<K, V>> leftChild = currentVertex.getLeftVertex();

        // If parent is null, currentVertex is the root of the entire tree.
        BinaryTreeVertex<Element<K, V>> rotationParent = currentVertex.getParent();

        if (rotationParent == null) {
            root = leftChild;
            leftChild.setParent(null);
        } else if (rotationParent.getLeftVertex() == currentVertex) {
            rotationParent.setLeftVertex(leftChild);
        } else {
            rotationParent.setRightVertex(leftChild);
        }

        currentVertex.setLeftVertex(leftChild.getRightVertex());
        leftChild.setRightVertex(currentVertex);
    }

    private void rotateRightLeft(BinaryTreeVertex<Element<K, V>> rotationRoot) {
        rotateRight(rotationRoot.getRightVertex());
        rotateLeft(rotationRoot);
    }

    private void rotateLeftRight(BinaryTreeVertex<Element<K, V>> rotationRoot) {
        rotateLeft(rotationRoot.getLeftVertex());
        rotateRight(rotationRoot);
    }

    private void balanceTree(BinaryTreeVertex<Element<K, V>> currentVertex) {
        if (currentVertex == null) {
            return;
        }

        // TODO: This is not optimal! We do not need to measure tree on each level.
        // How to fix:
        // a) cache measured height in tree vertices and update it only if tree changes;
        // b) make this function return height of subtree it worked on after balancing and use it instead of measuring
        // corresponding subtree of parent vertex.
        int leftHeight = getTreeHeight(currentVertex.getLeftVertex());
        int rightHeight = getTreeHeight(currentVertex.getRightVertex());
        int heightDiff = rightHeight - leftHeight;

        // In a correct AVL tree single addition or removal cannot create a height difference greater than 2.
        // That means that heightDiff cannot be less than -2 or greater than 2.

        if (heightDiff == -2) {
            // Left subtree is too high
            int leftLeftHeight = getTreeHeight(currentVertex.getLeftVertex().getLeftVertex());
            int leftRightHeight = getTreeHeight(currentVertex.getLeftVertex().getRightVertex());

            if (leftLeftHeight >= leftRightHeight) {
                rotateRight(currentVertex);
            } else {
                rotateLeftRight(currentVertex);
            }
        } else if (heightDiff == 2) {
            // Left subtree is too high
            int rightLeftHeight = getTreeHeight(currentVertex.getRightVertex().getLeftVertex());
            int rightRightHeight = getTreeHeight(currentVertex.getRightVertex().getRightVertex());

            if (rightLeftHeight <= rightRightHeight) {
                rotateLeft(currentVertex);
            } else {
                rotateRightLeft(currentVertex);
            }
        }
    }

    @Override
    protected void balanceFromVertexToRoot(BinaryTreeVertex<Element<K, V>> vertex) {
        while (vertex != null) {
            balanceTree(vertex);
            vertex = vertex.getParent();
        }
    }
}
