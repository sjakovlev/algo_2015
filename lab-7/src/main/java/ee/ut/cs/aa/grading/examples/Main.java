package ee.ut.cs.aa.grading.examples;

import ee.ut.cs.aa.grading.dijkstra.Path;
import ee.ut.cs.aa.grading.dijkstra.ShortestPathsFromVertex;
import ee.ut.cs.aa.grading.graph.Graph;

import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Graph<String, Integer> g = new GraphImpl<>();
        g.addVertex("A");
        g.addVertex("B");
        g.addVertex("C");
        g.addVertex("D");
        g.addEdge("A", "D", 20);
        g.addEdge("A", "B", 3);
        g.addEdge("B", "C", 5);
        g.addEdge("B", "D", 7);
        g.addEdge("C", "D", 1);

        ShortestPathsFromVertex<String, Integer> s = new DijkstraImpl<>();

        Map<String, Path<String>> res =
                s.getShortestPathsFromVertex(g, "A", edge -> edge);
    }
}
