package ee.ut.cs.aa.grading.examples;

import ee.ut.cs.aa.grading.dijkstra.Path;
import ee.ut.cs.aa.grading.dijkstra.ShortestPathsFromVertex;
import ee.ut.cs.aa.grading.graph.Cost;
import ee.ut.cs.aa.grading.graph.Graph;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class DijkstraImpl<V, E> implements ShortestPathsFromVertex<V, E> {
    @Override
    public Map<V, Path<V>> getShortestPathsFromVertex(
            Graph<V, E> graph,
            V startingVertex,
            Cost<E> costFunction) {

        PriorityQueue<Part> pq = new PriorityQueue<>();
        Map<V, Part> checked = new HashMap<>();

        pq.add(new Part(startingVertex, null, 0));

        while (!pq.isEmpty()) {
            
            Part currentVertex = pq.poll();

            for (V v : graph.neighborsOf(currentVertex.v)) {
                if (!checked.containsKey(v)) {
                    pq.add(new Part(v, currentVertex.v, currentVertex.cost +
                            costFunction.cost(graph.getEdge(currentVertex.v, v))));
                }
            }
        }

        return null;
    }

    private class Part implements Comparable<Part> {
        final V v;
        final V prevV;
        final double cost;

        public Part(V v, V prevV, double cost) {
            this.v = v;
            this.prevV = prevV;
            this.cost = cost;
        }

        @Override
        public int compareTo(Part o) {
            return Double.compare(cost, o.cost);
        }
    }
}
