package ee.ut.cs.aa.grading.examples;

import ee.ut.cs.aa.grading.heap.MinBinaryHeap;

public class MinBinaryHeapArrayImplTest extends MinBinaryHeapTest {

    @Override
    protected MinBinaryHeap<Integer> createHeap() {
        return new MinBinaryHeapArrayImpl<>();
    }
}