package ee.ut.cs.aa.grading.examples;

import ee.ut.cs.aa.grading.heap.MinBinaryHeap;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public abstract class MinBinaryHeapTest {

    private MinBinaryHeap<Integer> heap;
    private List<Integer> input;
    private int inputMin;
    private int inputMax;
    private Random rnd = new Random(System.currentTimeMillis());

    private List<Integer> getList(int n) {
        List<Integer> list = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            list.add(rnd.nextInt(n));
        }
        return list;
    }

    private <T extends Comparable<T>> boolean areAllItemsInHeap(
            MinBinaryHeap<T> heap, Collection<T> reference) {
        List<T> heapList = new ArrayList<>(heap.getArray());
        List<T> referenceList = new ArrayList<>(reference);
        Collections.sort(heapList);
        Collections.sort(referenceList);
        return heapList.equals(referenceList);
    }

    private <T extends Comparable<T>> boolean isHeap(List<T> heapState) {
        int maxInd = heapState.size() / 2;

        for (int i = 0; i < maxInd; i++) {
            int left = i * 2 + 1;
            int right = left + 1;

            if (left < heapState.size() &&
                    heapState.get(i).compareTo(heapState.get(left)) > 0) {
                return false;
            }
            if (right < heapState.size() &&
                    heapState.get(i).compareTo(heapState.get(right)) > 0) {
                return false;
            }
        }

        return true;
    }

    protected abstract MinBinaryHeap<Integer> createHeap();

    @Before
    public void setUp() throws Exception {
        final int N = 5;
        heap = createHeap();
        input = getList(N);
        inputMin = 0;
        inputMax = N;
    }

    @org.junit.Test
    public void testMinHeapify() throws Exception {
        heap.minHeapify(input);
        assertTrue(areAllItemsInHeap(heap, input));
    }

    @Test
    public void testMinHeapifyEmpty() throws Exception {
        heap.minHeapify(Collections.<Integer>emptyList());
        assertTrue(heap.isEmpty());
        assertNull(heap.peek());
        assertTrue(heap.getArray().isEmpty());
    }

    @Test
    public void testExtractMin() throws Exception {
        heap.minHeapify(input);
        List<Integer> extracted = new ArrayList<>(input.size());
        while (!heap.isEmpty()) {
            extracted.add(heap.extractMin());
        }

        List<Integer> sortedInput = new ArrayList<>(input);
        Collections.sort(sortedInput);
        assertTrue(sortedInput.equals(extracted));
        assertNull(heap.extractMin());
    }

    @Test
    public void testExtractMinEmpty() throws Exception {
        assertNull(heap.extractMin());
    }

    @Test
    public void testPeek() throws Exception {
        heap.minHeapify(input);
        List<Integer> sortedInput = new LinkedList<>(input);
        Collections.sort(sortedInput);
        while (!heap.isEmpty()) {
            Integer val = heap.peek();
            assertEquals(sortedInput.remove(0), val);
            heap.extractMin();
        }
        assertNull(heap.peek());
    }

    @Test
    public void testPeekEmpty() throws Exception {
        assertNull(heap.peek());
    }

    @Test
    public void testAdd() throws Exception {
        for (Integer integer : input) {
            heap.add(integer);
        }
        assertTrue(areAllItemsInHeap(heap, input));
    }

    @Test
    public void testIsEmpty() throws Exception {
        assertTrue(heap.isEmpty());
        for (Integer integer : input) {
            heap.add(integer);
            assertFalse(heap.isEmpty());
        }
        while (!heap.isEmpty()){
            heap.extractMin();
        }
        assertTrue(heap.isEmpty());
    }

    @Test
    public void testRemove() throws Exception {
        heap.minHeapify(input);
        List<Integer> removeOrder = new ArrayList<>();
        for (int i = inputMin; i < inputMax; i++) {
            removeOrder.add(i);
        }
        Collections.shuffle(removeOrder);

        for (Integer integer : removeOrder) {
            while (input.contains(integer)) {
                assertTrue(heap.remove(integer));
                input.remove(integer);
            }
            assertFalse(heap.remove(integer));
        }

        assertTrue(heap.isEmpty());
    }

    @Test
    public void testRemoveEmpty() throws Exception {
        assertFalse(heap.remove(0));
    }

    @Test
    public void testRemoveElementMovesUp(){
        heap.add(1);
        heap.add(10);
        heap.add(2);
        heap.add(12);
        heap.add(13);
        heap.add(3);
        heap.remove(12);
        assertTrue(isHeap(heap.getArray()));
    }

    @Test
    public void testContains() throws Exception {
        heap.minHeapify(input);
        List<Integer> sortedInput = new LinkedList<>(input);
        Collections.sort(sortedInput);
        for (int i = inputMin; i < inputMax; i++) {
            while (!sortedInput.isEmpty() && sortedInput.get(0) == i) {
                assertTrue(heap.contains(i));
                heap.extractMin();
                sortedInput.remove(0);
            }
            assertFalse(heap.contains(i));
        }
        assertTrue(heap.isEmpty());
    }

    @Test
    public void testContainsEmpty() throws Exception {
        assertFalse(heap.contains(0));
    }
}