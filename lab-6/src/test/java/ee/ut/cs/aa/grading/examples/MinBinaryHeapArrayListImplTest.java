package ee.ut.cs.aa.grading.examples;

import ee.ut.cs.aa.grading.heap.MinBinaryHeap;

public class MinBinaryHeapArrayListImplTest extends MinBinaryHeapTest {
    @Override
    protected MinBinaryHeap<Integer> createHeap() {
        return new MinBinaryHeapArrayListImpl<>();
    }
}
