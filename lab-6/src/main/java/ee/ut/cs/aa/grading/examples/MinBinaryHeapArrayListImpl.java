package ee.ut.cs.aa.grading.examples;

import ee.ut.cs.aa.grading.heap.MinBinaryHeap;

import java.util.*;

public class MinBinaryHeapArrayListImpl<T extends Comparable<T>>
        implements MinBinaryHeap<T> {

    /**
     * ArrayList that will contain references to elements stored in the heap.
     */
    private ArrayList<T> heap = new ArrayList<>();

    /**
     * HashMap that is needed to implement {@link #contains(Comparable)}
     * and {@link #remove(Comparable)} methods. Not needed if those operations
     * are not implemented.
     *
     * This maps value contained in the heap to a set of positions in the heap
     * (null or empty set if value is not in the heap, set of multiple indices
     * if the same value was added multiple times).
     *
     * This map has to be updated when any change to heap is made.
     */
    private HashMap<T, Set<Integer>> indexMap = new HashMap<>();

    /**
     * This helper method will allow us to get an existing set of indices from
     * the index map or create a new one and add it to the map if it does not
     * exist. This is useful if we are adding new elements to the heap.
     *
     * @param value value which indices we need to modify
     * @return set of indices of the given value
     */
    private Set<Integer> getOrCreateIndexSet(T value) {
        Set<Integer> indices = indexMap.get(value);
        if (indices == null) {
            indices = new HashSet<>();
            indexMap.put(value, indices);
        }
        return indices;
    }

    /**
     * Swap two elements of a heap.
     *
     * @param a index of an element
     * @param b index of an element
     */
    private void swap(int a, int b) {
        if (a == b) {
            return;
        }

        T valueA = heap.get(a);
        T valueB = heap.get(b);
        heap.set(a, valueB);
        heap.set(b, valueA);

        // Update the index map. By updating it here we avoid having to do it in
        // the bubbleUp and bubbleDown methods.
        Set<Integer> indicesA = indexMap.get(valueA);
        indicesA.remove(a);
        indicesA.add(b);
        Set<Integer> indicesB = indexMap.get(valueB);
        indicesB.remove(b);
        indicesB.add(a);
    }

    /**
     * Move element down the heap to its correct position.
     *
     * @param i index of an element that will be moved
     */
    private void bubbleDown(int i) {
        int leftChild = 2 * i + 1;
        int rightChild = leftChild + 1;

        int smallest = i;

        if (leftChild < heap.size() &&
                heap.get(leftChild).compareTo(heap.get(smallest)) < 0) {
            smallest = leftChild;
        }

        if (rightChild < heap.size() &&
                heap.get(rightChild).compareTo(heap.get(smallest)) < 0) {
            smallest = rightChild;
        }

        if (smallest != i) {
            swap(smallest, i);
            bubbleDown(smallest);
        }
    }

    /**
     * Move element up the heap to its correct position.
     *
     * @param i index of an element that will be moved
     */
    private void bubbleUp(int i) {
        if (i == 0) return;

        int parent = (i - 1) / 2;

        while (heap.get(i).compareTo(heap.get(parent)) < 0) {
            // Move current element up.
            swap(i, parent);
            i = parent;
            parent = (i - 1) / 2;
        }
    }

    @Override
    public String toString() {
        return heap.toString();
    }

    @Override
    public void minHeapify(List<T> array) {
        heap = new ArrayList<>(array);

        // Update the index map. Since we just completely replaced the entire
        // heap, we are going to also use a new map.
        indexMap = new HashMap<>();

        // Now add indices of new elements.
        int index = 0;
        for (T value : heap) {
            getOrCreateIndexSet(value).add(index++);
        }

        if (heap.size() <= 1) {
            // Empty array or just one element - no need to do anything else.
            return;
        }

        // Find the last index of an element on a level above the last one.
        int completeTreeCount = 3;
        while (completeTreeCount < heap.size()) {
            completeTreeCount = completeTreeCount * 2 + 1;
        }

        int startIndex = completeTreeCount / 2 - 1;

        for (int i = startIndex; i >= 0; i--) {
            bubbleDown(i);
        }
    }

    /**
     * Remove element with the given index from the heap and return it.
     *
     * This method is used to implement both {@link #extractMin()} and
     * {@link #remove(Comparable)} methods.
     *
     * @param index position of an element that has to be removed
     * @return removed element or null if the element was not in the heap
     */
    private T extract(int index) {
        if (index < 0 || index >= heap.size()) {
            // Out of bounds. Return null.
            // Throwing exception may be semantically better, but this is faster
            // and is fine in this case.
            return null;
        }

        int lastIndex = heap.size() - 1;

        swap(index, lastIndex);
        T removedValue = heap.remove(lastIndex);

        // Update the index map.
        indexMap.get(removedValue).remove(lastIndex);

        if (index == lastIndex) {
            // Just removed the last item, no need to fix anything.
            return removedValue;
        }

        // Correct position of the element.
        bubbleDown(index);
        bubbleUp(index);    // Needed if the removed element is not root and
                            // it was moved to another branch.

        return removedValue;
    }

    @Override
    public T extractMin() {
        // Just remove and return the first item in the heap.
        return extract(0);
    }

    @Override
    public T peek() {
        if (heap.isEmpty()) {
            return null;
        }

        return heap.get(0);
    }

    @Override
    public void add(T element) {
        heap.add(element);

        // Update the index map
        getOrCreateIndexSet(element).add(heap.size() - 1);

        bubbleUp(heap.size() - 1);
    }

    @Override
    public boolean isEmpty() {
        return heap.isEmpty();
    }

    /*
     * The following methods are often not implemented for binary heaps
     * as they require extra space and operations to realize.
     */

    @Override
    public boolean remove(T element) {
        Set<Integer> indices = indexMap.get(element);
        if (indices == null || indices.isEmpty()) {
            // No such element, just return false.
            return false;
        }

        // Select some index (do not care which one exactly).
        int index = indices.stream().findFirst().get();

        // Remove found item using extract() method.
        return extract(index) != null;
    }

    @Override
    public boolean contains(T element) {
        Set<Integer> indices = indexMap.get(element);
        return !(indices == null || indices.isEmpty());
    }

    /*
     * The following methods are used only to test the solution.
     */

    @Override
    public List<T> getArray() {
        // Do not let the heap to change outside this class.
        return Collections.unmodifiableList(heap);
    }

    public Map<T, Set<Integer>> getIndexMap() {
        return Collections.unmodifiableMap(indexMap);
    }
}
