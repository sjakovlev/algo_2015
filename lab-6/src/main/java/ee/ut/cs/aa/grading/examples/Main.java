package ee.ut.cs.aa.grading.examples;

import ee.ut.cs.aa.grading.heap.MinBinaryHeap;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

    public static final Random RANDOM = new Random(System.currentTimeMillis());

    private static List<Integer> genRandomList(int n) {
        final List<Integer> data = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            data.add(RANDOM.nextInt(n));
        }
        return data;
    }

    public static void main(String[] args) {
        MinBinaryHeap<Integer> heap = new MinBinaryHeapArrayListImpl<>();
        heap.minHeapify(genRandomList(20));

        System.out.println(heap);

        while (!heap.isEmpty()) {
            System.out.println(heap.extractMin());
        }
    }
}
