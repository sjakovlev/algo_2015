package ee.ut.cs.aa.grading.examples;

import ee.ut.cs.aa.grading.heap.MinBinaryHeap;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@SuppressWarnings({"unchecked"})
public class MinBinaryHeapArrayImpl<T extends Comparable<T>> implements MinBinaryHeap<T> {

    /**
     * Array that will contain references to elements.
     */
    private Comparable heap[]= new Comparable[0];

    /**
     * Number of elements added to the heap.
     * Index of the first free spot in the heap array.
     */
    private int count = 0;

    /**
     * Resize heap keeping already added values.
     *
     * @param newSize new heap size
     */
    private void resize(int newSize) {
        if (newSize < count) {
            throw new RuntimeException("Cannot resize heap - new size is smaller than element count");
        }

        heap = Arrays.copyOf(heap, newSize);
    }

    /**
     * Swap two elements of a heap.
     *
     * @param a index of an element
     * @param b index of an element
     */
    private void swap(int a, int b) {
        Comparable value = heap[a];
        heap[a] = heap[b];
        heap[b] = value;
    }

    /**
     * Heapify subtree rooted at element with given index.
     *
     * @param i index of the root element of a subtree that will be heapified
     */
    private void minHeapify(int i) {
        int leftChild = 2 * i + 1;
        int rightChild = leftChild + 1;

        int smallest = i;

        if (leftChild < count && heap[leftChild].compareTo(heap[smallest]) < 0) {
            smallest = leftChild;
        }

        if (rightChild < count && heap[rightChild].compareTo(heap[smallest]) < 0) {
            smallest = rightChild;
        }

        if (smallest != i) {
            swap(smallest, i);
            minHeapify(smallest);
        }
    }

    /**
     * Move element up the heap to its correct position.
     *
     * @param i index of an element that will be moved
     */
    private void bubbleUp(int i) {
        if (i == 0) return;

        int parent = (i - 1) / 2;

        while (heap[i].compareTo(heap[parent]) < 0) {
            // Move current element up.
            swap(i, parent);
            i = parent;
            parent = (i - 1) / 2;
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        if (count > 0) {
            sb.append(heap[0].toString());
        }
        for (int i = 1; i < count; i++) {
            sb.append(", ");
            sb.append(heap[i].toString());
        }
        sb.append("]");
        return sb.toString();
    }

    @Override
    public void minHeapify(List<T> array) {
        heap = array.toArray(heap);
        count = array.size();

        if (count <= 1) {
            // Empty array or just one element - no need to do anything.
            return;
        }

        // Find the last index of an element on a level above the last one.
        int completeTreeCount = 3;
        while (completeTreeCount < count) {
            completeTreeCount = completeTreeCount * 2 + 1;
        }

        int startIndex = completeTreeCount / 2 - 1;

        for (int i = startIndex; i >= 0; i--) {
            minHeapify(i);
        }
    }

    @Override
    public T extractMin() {
        if (count < 1) {
            return null;
        }

        T minVal = (T) heap[0];
        count--;
        swap(0, count);
        minHeapify(0);

        return minVal;
    }

    @Override
    public T peek() {
        if (count < 1) {
            return null;
        }
        return (T) heap[0];
    }

    @Override
    public void add(T element) {
        if (count >= heap.length) {
            // Grow heap by adding another tree level.
            // For a complete binary tree with N elements, next level will have N+1 elements.
            // The tree in total will have N + (N + 1) = 2 * N + 1 elements.
            resize(2 * count + 1);
        }
        heap[count] = element;
        bubbleUp(count);
        count++;
    }

    @Override
    public boolean isEmpty() {
        return count == 0;
    }

    @Override
    public boolean remove(T element) {
        // TODO
        throw new NotImplementedException();
    }

    @Override
    public boolean contains(T element) {
        // TODO
        throw new NotImplementedException();
    }

    @Override
    public List<T> getArray() {
        if (count == 0) {
            return Collections.emptyList();
        }
        return ((List) Arrays.asList(heap)).subList(0, count);
    }
}
