import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import ee.ut.cs.aa.grading.lab_1.plotter.TimePlot;
import ee.ut.cs.aa.grading.lab_1.plotter.TimePlotImplementation;

public class Main {
	
	private static double calculateAverage(double[] values) {
		double average = 0;
		for (int i = 0; i < values.length; i++) {
			average += values[i];
		}
		average /= values.length;
		return average;
	}
	
	private static double measureDumbSortAvg(int length, int repeats) {
		DumbSort<Integer> dumbSort = new DumbSort<>();
		double results[] = new double[repeats];
		
		for (int i = 0; i < repeats; i++) {
			List<Integer> input = generateShuffledList(length);
			
			long startTime = System.nanoTime();
			dumbSort.sort(input);
			long elapsedTime = System.nanoTime() - startTime;
			
			results[i] = elapsedTime;
		}
		
		return calculateAverage(results);
	}

	private static ArrayList<Integer> generateShuffledList(int length) {
		ArrayList<Integer> list = new ArrayList<Integer>();

		for (int i = 0; i < length; i++) {
			list.add(i);
		}
		Collections.shuffle(list);
		return list;
	}
	
	private static XYSeries measureSeriesShuffled(int[] inputLengths, int measures) {
		XYSeries series = new XYSeries("ShuffledDumbSort");
		
		for (int i = 0; i < inputLengths.length; i++) {
			int length = inputLengths[i];
			double time = measureDumbSortAvg(length, measures);
			series.add(length, time);
		}
		
		return series;
	}
	
	public static void main(String[] args) {
		// Input lists of those lengths will be used to measure sorter performance.
		final int[] inputLengths = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
		
		// Each measurement will be repeated this many times. Results will be averaged.
		final int measureCount = 30;
		
		// Warm-up, results of this run are ignored.
		// See https://bitbucket.org/sjakovlev/algo_2015/wiki/Praktikumid%201-2
		measureSeriesShuffled(inputLengths, measureCount);

		// Series collection can be used to store series of measurements.
		// Each series in the collection corresponds to a separate plot (so,
		// separate series have to be added for each algorithm that has to be
		// compared).
		XYSeriesCollection seriesCollection = new XYSeriesCollection();
		
		XYSeries dumbSortShuffledSeries = measureSeriesShuffled(inputLengths, measureCount);
		seriesCollection.addSeries(dumbSortShuffledSeries);
		
		// Series must be added for each tested sorting method:
		//XYSeries betterSortShuffledSeries = ...;
		//seriesCollection.addSeries(betterSortShuffledSeries);

		// Display time plot with collected series.
		TimePlot timePlot = new TimePlotImplementation();
		timePlot.plot(seriesCollection);
	}
}
