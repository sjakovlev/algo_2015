import java.util.ArrayList;
import java.util.List;

import ee.ut.cs.aa.grading.lab_1.sorting.Sorter;

/**
 * Very bad example of {@link Sorter}. Never use in practice. Ever.
 * 
 * @author Sergei Jakovlev
 * @param <T> type of objects that can be sorted by DumbSort instance
 */
public class DumbSort<T extends Comparable<T>> implements Sorter<T> {
	
	private T findMin(List<T> list) {
		T currentlyFoundMin = null;
		for (T listItem : list) {
			if (currentlyFoundMin == null || listItem.compareTo(currentlyFoundMin) < 0) {
				currentlyFoundMin = listItem;
			}
		}
		
		return currentlyFoundMin;
	}

	@Override
	public void sort(List<T> list) {
		List<T> copyOfList = new ArrayList<>(list);
		list.clear();
		// list is now empty
		while(!copyOfList.isEmpty()) {
			// Find smallest value stored in copy of the list,
			// remove it from the copy and add to end of initial list.
			T min = findMin(copyOfList);
			copyOfList.remove(min);
			list.add(min);
		}
	}
}
