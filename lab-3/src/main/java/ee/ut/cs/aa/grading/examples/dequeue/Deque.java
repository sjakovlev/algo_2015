package ee.ut.cs.aa.grading.examples.dequeue;

import java.util.NoSuchElementException;

public class Deque<T> {
    private Node<T> first;
    private Node<T> last;

    public boolean isEmpty() {
        return first == null;
    }

    public boolean isLastRemaining() {
        return first == last;
    }

    public void pushFirst(T item) {
        Node<T> newNode = new Node<>(item);

        if (isEmpty()) {
            first = newNode;
            last = newNode;
            return;
        }

        newNode.setNext(first);
        first.setPrevious(newNode);
        first = newNode;
    }

    public void pushLast(T item) {
        Node<T> newNode = new Node<>(item);

        if (isEmpty()) {
            first = newNode;
            last = newNode;
            return;
        }

        newNode.setPrevious(last);
        last.setNext(newNode);
        last = newNode;
    }

    public T peekFirst() throws NoSuchElementException {
        if (isEmpty()) {
            throw new NoSuchElementException("Cannot peek into an empty Deque");
        }
        return first.getData();
    }

    public T peekLast() throws NoSuchElementException {
        if (isEmpty()) {
            throw new NoSuchElementException("Cannot peek into an empty Deque");
        }
        return last.getData();
    }

    public T popFirst() throws NoSuchElementException {
        if (isEmpty()) {
            throw new NoSuchElementException("Cannot pop from an empty Deque");
        }

        if (isLastRemaining()) {
            Node<T> removed = first;
            first = null;
            last = null;
            return removed.getData();
        }

        Node<T> removed = first;
        first.getNext().setPrevious(null);
        first = first.getNext();
        return removed.getData();
    }

    public T popLast() throws NoSuchElementException {
        if (isEmpty()) {
            throw new NoSuchElementException("Cannot pop from an empty Deque");
        }

        if (isLastRemaining()) {
            Node<T> removed = first;
            first = null;
            last = null;
            return removed.getData();
        }

        Node<T> removed = last;
        last.getPrevious().setNext(null);
        last = last.getPrevious();
        return removed.getData();
    }

    private class Node<T> {
        private T data;

        private Node<T> previous;
        private Node<T> next;

        public Node(T data) {
            this.data = data;
        }

        public T getData() {
            return data;
        }

        public void setData(T data) {
            this.data = data;
        }

        public Node<T> getPrevious() {
            return previous;
        }

        public void setPrevious(Node<T> previous) {
            this.previous = previous;
        }

        public Node<T> getNext() {
            return next;
        }

        public void setNext(Node<T> next) {
            this.next = next;
        }
    }
}
