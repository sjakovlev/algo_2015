package ee.ut.cs.aa.grading.examples;

import ee.ut.cs.aa.grading.tree.BinaryTreeVertex;
import ee.ut.cs.aa.grading.tree.util.Drawer;
import ee.ut.cs.aa.grading.tree.util.DrawerImpl;

public class Main {
    public static void main(String[] args) {
        BinaryTreeVertex<Integer> root = new BinaryTreeVertex<>(3);
        BinaryTreeVertex<Integer> left = new BinaryTreeVertex<>(1);
        BinaryTreeVertex<Integer> right = new BinaryTreeVertex<>(5);
        BinaryTreeVertex<Integer> left_right = new BinaryTreeVertex<>(2);

        root.setLeftVertex(left);
        root.setRightVertex(right);
        left.setRightVertex(left_right);

        Drawer d = new DrawerImpl();
        d.draw(root);
    }
}
