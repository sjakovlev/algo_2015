package ee.ut.cs.aa.grading.examples.dequeue;

import ee.ut.cs.aa.grading.dequeue.Stack;

import java.util.NoSuchElementException;

public class StackLinkedListImpl<T> implements Stack<T> {

    private LinkedListNode<T> first;

    @Override
    public boolean isEmpty() {
        return first == null;
    }

    @Override
    public void push(T item) {
        LinkedListNode<T> newNode = new LinkedListNode<>(item);
        newNode.setNext(first);
        first = newNode;
    }

    @Override
    public T pop() throws NoSuchElementException {
        if (isEmpty()) {
            throw new NoSuchElementException("Cannot pop from empty stack");
        }

        LinkedListNode<T> removedNode = first;
        first = first.getNext();
        return removedNode.getItem();
    }

    @Override
    public T peek() throws NoSuchElementException {
        if (isEmpty()) {
            throw new NoSuchElementException("Cannot peek into empty stack");
        }

        return first.getItem();
    }

    private static class LinkedListNode<T> {
        private final T item;
        private LinkedListNode<T> next;

        public LinkedListNode(T item) {
            this.item = item;
        }

        public T getItem() {
            return item;
        }

        public LinkedListNode<T> getNext() {
            return next;
        }

        public void setNext(LinkedListNode<T> next) {
            this.next = next;
        }
    }
}
