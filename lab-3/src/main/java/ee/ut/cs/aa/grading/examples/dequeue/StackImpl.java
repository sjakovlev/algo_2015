package ee.ut.cs.aa.grading.examples.dequeue;

import ee.ut.cs.aa.grading.dequeue.Stack;

import java.util.NoSuchElementException;

public class StackImpl<T> extends Deque<T> implements Stack<T> {
    @Override
    public void push(T item) {
        pushFirst(item);
    }

    @Override
    public T pop() throws NoSuchElementException {
        return popFirst();
    }

    @Override
    public T peek() throws NoSuchElementException {
        return peekFirst();
    }
}
