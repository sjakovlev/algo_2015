package ee.ut.cs.aa.grading.examples.dequeue;

import ee.ut.cs.aa.grading.dequeue.Queue;

import java.util.NoSuchElementException;

public class QueueImpl<T> extends Deque<T> implements Queue<T> {
    @Override
    public void add(T item) {
        pushFirst(item);
    }

    @Override
    public T remove() throws NoSuchElementException {
        return popLast();
    }

    @Override
    public T peek() throws NoSuchElementException {
        return peekLast();
    }
}
