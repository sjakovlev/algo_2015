package ee.ut.cs.aa.grading.examples;

import ee.ut.cs.aa.grading.graph.Graph;
import ee.ut.cs.aa.grading.lab_8.MinimumSpanningTreeAlgorithm;

public class Main {
    public static void main(String[] args) {
        Graph<String, Integer> g = new GraphImpl<>();

        g.addVertex("A");
        g.addVertex("B");
        g.addVertex("C");
        g.addVertex("D");
        g.addVertex("E");
        g.addVertex("F");

        g.addEdge("A", "B", 1);
        g.addEdge("A", "D", 4);
        g.addEdge("A", "E", 3);
        g.addEdge("B", "D", 4);
        g.addEdge("B", "E", 2);
        g.addEdge("C", "E", 4);
        g.addEdge("C", "F", 5);
        g.addEdge("D", "E", 4);
        g.addEdge("E", "F", 7);

        MinimumSpanningTreeAlgorithm<String, Integer> msta = new PrimImpl<>();

        Graph<String, Integer> spanningTree =
                msta.findMinimumSpanningTree(g, Integer::doubleValue);

        for (String s : spanningTree.getVertices()) {
            System.out.println(s);
            for (String n : spanningTree.neighborsOf(s)) {
                System.out.println(s + " -> " + n +
                        " (" + spanningTree.getEdge(s, n) + ")");
            }
        }
    }
}
