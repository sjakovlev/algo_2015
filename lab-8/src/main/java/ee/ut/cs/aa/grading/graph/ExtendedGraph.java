package ee.ut.cs.aa.grading.graph;

import java.util.Set;

public interface ExtendedGraph<V, E> extends Graph<V, E> {

    Set<MetaEdge<V, E>> getEdges();

    class MetaEdge<V, E> {
        public final V v1;
        public final V v2;
        public final E e;

        public MetaEdge(V v1, V v2, E e) {
            this.v1 = v1;
            this.v2 = v2;
            this.e = e;
        }
    }
}
