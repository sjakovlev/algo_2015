package ee.ut.cs.aa.grading.examples;

import ee.ut.cs.aa.grading.graph.Graph;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class GraphImpl<V, E> implements Graph<V, E> {

    private final Map<V, Map<V, E>> adj = new HashMap<>();

    @Override
    public boolean addVertex(V vertex) {
        if (adj.containsKey(vertex)) return false;
        adj.put(vertex, new HashMap<>());
        return true;
    }

    @Override
    public boolean removeVertex(V vertex) {
        if (!adj.containsKey(vertex)) return false;
        for (Map<V, E> veMap : adj.values()) {
            veMap.remove(vertex);
        }
        adj.remove(vertex);
        return true;
    }

    @Override
    public Set<V> getVertices() {
        return adj.keySet();
    }

    @Override
    public boolean addEdge(V one, V other, E edge) {
        Map<V, E> oneEdges = adj.get(one);
        Map<V, E> otherEdges = adj.get(other);

        if (oneEdges == null || otherEdges == null ||
                oneEdges.containsKey(other)) {
            return false;
        }

        oneEdges.put(other, edge);
        otherEdges.put(one, edge);
        return true;
    }

    @Override
    public boolean removeEdge(V one, V other) {
        Map<V, E> oneEdges = adj.get(one);
        Map<V, E> otherEdges = adj.get(other);

        return !(oneEdges == null || otherEdges == null) &&
                oneEdges.remove(other) != null &&
                otherEdges.remove(one) != null;
    }

    @Override
    public Set<V> neighborsOf(V vertex) {
        Map<V, E> vAdj = adj.get(vertex);
        if (vAdj == null) return null;

        return vAdj.keySet();
    }

    @Override
    public E getEdge(V one, V other) {
        Map<V, E> oneEdges = adj.get(one);
        if (oneEdges == null) return null;
        return oneEdges.get(other);
    }
}
