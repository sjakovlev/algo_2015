package ee.ut.cs.aa.grading.examples;

import ee.ut.cs.aa.grading.graph.Cost;
import ee.ut.cs.aa.grading.graph.Graph;
import ee.ut.cs.aa.grading.lab_8.MinimumSpanningTreeAlgorithm;

import java.util.*;

/**
 * Implementation of Prim's algorithm.
 *
 * @param <V> type of vertices
 * @param <E> type of edges
 */
public class PrimImpl<V, E> implements MinimumSpanningTreeAlgorithm<V, E> {

    @Override
    public Graph<V, E> findMinimumSpanningTree(Graph<V, E> graph,
                                               Cost<E> costFunction) {

        /*
         * Data structures
         */

        // Priority queue of all currently available edges.
        PriorityQueue<Edge> queue = new PriorityQueue<>(
                new EdgeComparator(costFunction));

        // Set of vertices that are already in the tree.
        Set<V> addedVertices = new HashSet<>();

        // List of edges that are in the spanning tree.
        List<Edge> addedEdges = new LinkedList<>();

        /*
         * Algorithm
         */

        // Count vertices.
        // If graph is empty, return null immediately.
        // When all vertices are added to the tree, spanning tree is completed.
        final int vertexCount = graph.getVertices().size();

        if (vertexCount == 0) {
            return null;
        }

        // Select initial vertex and add it to spanning tree.
        V startVertex = graph.getVertices().stream().findAny().get();
        addedVertices.add(startVertex);

        // Add all edges of the initial vertex to the queue.
        Set<V> startVxNeighbours = graph.neighborsOf(startVertex);
        for (V startVxNeighbour : startVxNeighbours) {
            queue.add(new Edge(
                    graph.getEdge(startVertex, startVxNeighbour),
                    startVertex,
                    startVxNeighbour));
        }

        while (addedVertices.size() < vertexCount) {

            if (queue.isEmpty()) {
                // Not all vertices are reached, but there are no more available
                // edges. This means that the graph is not completely connected.
                // Return null.

                return null;
            }

            // Select the edge with the lowest weight (cost) from the queue.
            Edge edge = queue.poll();

            // If it goes to an already added vertex, ignore it.
            if (addedVertices.contains(edge.to)) {
                continue;
            }

            // Otherwise, add this edge and the vertex it goes to spanning tree.
            addedEdges.add(edge);
            addedVertices.add(edge.to);

            // Add all edges of the newly added vertex to the queue.
            Set<V> neighbors = graph.neighborsOf(edge.to);
            for (V neighbor : neighbors) {
                if (!addedVertices.contains(neighbor)) {
                    queue.add(new Edge(
                            graph.getEdge(edge.to, neighbor),
                            edge.to,
                            neighbor));
                }
            }
        }

        // We have found all needed edges. Now build a graph from all vertices
        // and the list of edges.

        Graph<V, E> spanningTree = new GraphImpl<>();

        for (V addedVertex : addedVertices) {
            spanningTree.addVertex(addedVertex);
        }

        for (Edge addedEdge : addedEdges) {
            spanningTree.addEdge(addedEdge.from, addedEdge.to, addedEdge.edge);
        }

        return spanningTree;
    }

    /**
     * Wrapper for graph edges that will be used to store edges in the queue.
     * <p>
     * Since we cannot make any assumptions regarding what type E actually is,
     * we have to also store vertices the edge connects.
     */
    private class Edge {

        final E edge;
        final V from;
        final V to;

        /**
         * Create a new wrapped edge.
         *
         * @param edge edge itself
         * @param from vertex that already is in the spanning tree
         * @param to vertex that may not be in the spanning tree
         */
        Edge(E edge, V from, V to) {
            this.edge = edge;
            this.from = from;
            this.to = to;
        }
    }

    /**
     * Comparator that will sort edges using given cost function.
     */
    private class EdgeComparator implements Comparator<Edge> {

        private final Cost<E> costFunction;

        EdgeComparator(Cost<E> costFunction) {
            this.costFunction = costFunction;
        }

        @Override
        public int compare(Edge o1, Edge o2) {
            return Double.compare(costFunction.cost(o1.edge),
                    costFunction.cost(o2.edge));
        }
    }
}
