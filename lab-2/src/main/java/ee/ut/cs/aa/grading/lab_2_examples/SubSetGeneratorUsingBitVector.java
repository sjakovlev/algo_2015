package ee.ut.cs.aa.grading.lab_2_examples;

import ee.ut.cs.aa.grading.lab_2.combinatorics.SubSetGenerator;

import java.util.*;

public class SubSetGeneratorUsingBitVector<T> implements SubSetGenerator<T> {

    @Override
    public Set<Set<T>> allSubsets(Set<T> set) {
        Set<Set<T>> result = new HashSet<>();

        List<T> fixedSet = new ArrayList<>(set);

        Set<boolean[]> bitVectors = (new BitVectorGeneratorUpward()).bitVectors(set.size());
        for (boolean[] bitVector : bitVectors) {
            Set<T> subset = new HashSet<>();
            for (int i = 0; i < bitVector.length; i++) {
                if (bitVector[i]) {
                    subset.add(fixedSet.get(i));
                }
            }
            result.add(subset);
        }

        return result;
    }

    @Override
    public Set<Set<T>> allSubsetsOfSizeK(Set<T> set, int k) {
        if (k < 0 || k > set.size()) {
            throw new IllegalArgumentException("Subset size must be in range [0; set.size()]");
        }

        Set<Set<T>> subsets = allSubsets(set);
        Set<Set<T>> matchingSubsets = new HashSet<>();

        for (Set<T> subset : subsets) {
            if (subset.size() == k) {
                matchingSubsets.add(subset);
            }
        }

        return matchingSubsets;
    }
}
