package ee.ut.cs.aa.grading.lab_2_examples;

import ee.ut.cs.aa.grading.lab_2.combinatorics.PermutationGenerator;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class PermutationGeneratorAccum<T> implements PermutationGenerator<T> {

    private void addPermutations(List<T> available, List<T> permutationPart, Set<List<T>> result) {
        if (available.isEmpty()) {
            result.add(permutationPart);
            return;
        }

        for (int i = 0; i < available.size(); i++) {
            List<T> newAvailable = new LinkedList<>(available);
            T element = newAvailable.remove(i);
            List<T> newPermPart = new LinkedList<>(permutationPart);
            newPermPart.add(element);
            addPermutations(newAvailable, newPermPart, result);
        }
    }

    @Override
    public Set<List<T>> allPermutations(List<T> list) {
        Set<List<T>> result = new HashSet<>();
        addPermutations(list, new LinkedList<T>(), result);
        return result;
    }
}
