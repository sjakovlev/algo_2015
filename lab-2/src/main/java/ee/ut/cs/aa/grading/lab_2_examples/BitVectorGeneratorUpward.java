package ee.ut.cs.aa.grading.lab_2_examples;

import ee.ut.cs.aa.grading.lab_2.combinatorics.BitVectorGenerator;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class BitVectorGeneratorUpward implements BitVectorGenerator {

    private boolean[] append(boolean[] prefix, boolean newValue) {
        boolean[] result = Arrays.copyOf(prefix, prefix.length + 1);
        result[prefix.length] = newValue;
        return result;
    }

    public Set<boolean[]> bitVectors(int length) {
        if (length < 0) {
            throw new IllegalArgumentException("Length cannot be negative");
        }

        if (length == 0) {
            Set<boolean[]> result = new HashSet<>();
            result.add(new boolean[0]);
            return result;
        }

        Set<boolean[]> shortVectors = bitVectors(length - 1);

        Set<boolean[]> result = new HashSet<>();

        for (boolean[] shortVector : shortVectors) {
            result.add(append(shortVector, true));
            result.add(append(shortVector, false));
        }

        return result;
    }
}
