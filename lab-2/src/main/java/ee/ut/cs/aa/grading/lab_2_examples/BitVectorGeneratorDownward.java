package ee.ut.cs.aa.grading.lab_2_examples;

import ee.ut.cs.aa.grading.lab_2.combinatorics.BitVectorGenerator;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class BitVectorGeneratorDownward implements BitVectorGenerator {

    private Set<boolean[]> getFilledVectors(int remainingLength, boolean[] partialVector) {
        if (remainingLength < 0) {
            throw new IllegalArgumentException("Length cannot be less than zero");
        }

        if (remainingLength == 0) {
            Set<boolean[]> result = new HashSet<>();
            result.add(partialVector);
            return result;
        }

        boolean[] partialVectorCopy = Arrays.copyOf(partialVector, partialVector.length);
        partialVector[remainingLength - 1] = false;
        partialVectorCopy[remainingLength - 1] = true;

        Set<boolean[]> result = new HashSet<>();
        result.addAll(getFilledVectors(remainingLength - 1, partialVector));
        result.addAll(getFilledVectors(remainingLength - 1, partialVectorCopy));

        return result;
    }

    public Set<boolean[]> bitVectors(int length) {
        return getFilledVectors(length, new boolean[length]);
    }
}
