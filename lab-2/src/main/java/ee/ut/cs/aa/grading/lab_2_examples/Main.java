package ee.ut.cs.aa.grading.lab_2_examples;

import ee.ut.cs.aa.grading.lab_2.combinatorics.BitVectorGenerator;
import ee.ut.cs.aa.grading.lab_2.combinatorics.PermutationGenerator;
import ee.ut.cs.aa.grading.lab_2.combinatorics.SubSetGenerator;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
        runBitVectorExample(4);
        printSeparator();
        runSubSetExample(new HashSet<>(Arrays.asList(1, 2, 3)));
        printSeparator();
        runSubSetOfSizeKExample(new HashSet<>(Arrays.asList(1, 2, 3, 4)), 2);
        printSeparator();
        runPermutationExample(Arrays.asList(1, 2, 3));
    }

    private static void runBitVectorExample(int length) {
        System.out.println("Bit vectors of length " + length + ":");

        BitVectorGenerator bvga = new BitVectorGeneratorAccum();
        BitVectorGenerator bvgd = new BitVectorGeneratorDownward();
        BitVectorGenerator bvgu = new BitVectorGeneratorUpward();
        System.out.println("Using accumulator:             " +
                Utils.bitVectorSetToStringSorted(bvga.bitVectors(length)));
        System.out.println("Using downward implementation: " +
                Utils.bitVectorSetToStringSorted(bvgd.bitVectors(length)));
        System.out.println("Using upward implementation:   " +
                Utils.bitVectorSetToStringSorted(bvgu.bitVectors(length)));
    }

    private static void runSubSetExample(Set<Integer> inputSet) {
        System.out.println("Subsets of set " + inputSet.toString() + ":");

        SubSetGenerator<Integer> ssga = new SubSetGeneratorAccum<>();
        SubSetGenerator<Integer> ssgbv = new SubSetGeneratorUsingBitVector<>();
        System.out.println("Using accumulator: " + ssga.allSubsets(inputSet));
        System.out.println("Using bit vectors: " + ssgbv.allSubsets(inputSet));
    }

    private static void runSubSetOfSizeKExample(Set<Integer> inputSet, int k) {
        System.out.println("Subsets of set " + inputSet.toString() + " of size " + k + ":");

        SubSetGenerator<Integer> ssga = new SubSetGeneratorAccum<>();
        SubSetGenerator<Integer> ssgbv = new SubSetGeneratorUsingBitVector<>();
        System.out.println("Using accumulator: " + ssga.allSubsetsOfSizeK(inputSet, k));
        System.out.println("Using bit vectors: " + ssgbv.allSubsetsOfSizeK(inputSet, k));
    }

    private static void runPermutationExample(List<Integer> inputList) {
        System.out.println("Permutations of list " + inputList.toString() + ":");
        PermutationGenerator<Integer> pgu = new PermutationGeneratorUpward<>();
        PermutationGenerator<Integer> pga = new PermutationGeneratorAccum<>();
        System.out.println("Using upward implementation: " + pgu.allPermutations(inputList));
        System.out.println("Using accumulator:           " + pga.allPermutations(inputList));
    }

    private static void printSeparator() {
        System.out.println("====================");
    }
}
