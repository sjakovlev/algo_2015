package ee.ut.cs.aa.grading.lab_2_examples;

import ee.ut.cs.aa.grading.lab_2.combinatorics.PermutationGenerator;

import java.util.*;

public class PermutationGeneratorUpward<T> implements PermutationGenerator<T> {

    @Override
    public Set<List<T>> allPermutations(List<T> list) {
        Set<List<T>> result = new HashSet<>();

        if (list.size() <= 1) {
            result.add(list);
            return result;
        }

        for (int i = 0; i < list.size(); i++) {
            List<T> newList = new LinkedList<>(list);
            T selectedElement = (T) newList.remove(i);
            Set<List<T>> permutationsLeft = allPermutations(newList);
            for (List<T> permutationLeft : permutationsLeft) {
                permutationLeft.add(selectedElement);
                result.add(permutationLeft);
            }
        }

        return result;
    }
}
