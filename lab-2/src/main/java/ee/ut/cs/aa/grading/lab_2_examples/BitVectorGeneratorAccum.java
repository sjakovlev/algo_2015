package ee.ut.cs.aa.grading.lab_2_examples;

import ee.ut.cs.aa.grading.lab_2.combinatorics.BitVectorGenerator;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class BitVectorGeneratorAccum implements BitVectorGenerator {

    private void addVectorsToSet(int remainingLength, boolean[] partialVector, Set<boolean[]> resultSet) {
        if (remainingLength < 0) {
            throw new IllegalArgumentException("Length cannot be less than zero");
        }

        if (remainingLength == 0) {
            resultSet.add(partialVector);
            return;
        }

        boolean[] partialVectorCopy = Arrays.copyOf(partialVector, partialVector.length);
        partialVector[remainingLength - 1] = false;
        partialVectorCopy[remainingLength - 1] = true;

        addVectorsToSet(remainingLength - 1, partialVector, resultSet);
        addVectorsToSet(remainingLength - 1, partialVectorCopy, resultSet);
    }

    public Set<boolean[]> bitVectors(int length) {
        Set<boolean[]> resultSet = new HashSet<>();
        addVectorsToSet(length, new boolean[length], resultSet);
        return resultSet;
    }
}
