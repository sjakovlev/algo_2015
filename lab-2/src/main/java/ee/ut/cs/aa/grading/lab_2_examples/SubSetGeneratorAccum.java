package ee.ut.cs.aa.grading.lab_2_examples;

import ee.ut.cs.aa.grading.lab_2.combinatorics.SubSetGenerator;

import java.util.*;

public class SubSetGeneratorAccum<T> implements SubSetGenerator<T> {

    private void addSubSets(List<T> superSetElements, int position, Set<T> alreadyAdded, Set<Set<T>> subSets) {
        int elementsLeft = superSetElements.size() - position;

        if (elementsLeft <= 0) {
            subSets.add(alreadyAdded);
            return;
        }

        Set<T> alreadyAddedWithElement = new HashSet<>(alreadyAdded);
        alreadyAddedWithElement.add(superSetElements.get(position));

        addSubSets(superSetElements, position + 1, alreadyAdded, subSets);
        addSubSets(superSetElements, position + 1, alreadyAddedWithElement, subSets);
    }

    @Override
    public Set<Set<T>> allSubsets(Set<T> set) {
        Set<Set<T>> result = new HashSet<>();
        addSubSets(new ArrayList<T>(set), 0, new HashSet<T>(), result);
        return result;
    }

    private void addSubSetsOfSizeK(List<T> superSetElements, int position,
                                   Set<T> alreadyAdded, int needToAdd, Set<Set<T>> subSets) {

        if (needToAdd == 0) {
            subSets.add(alreadyAdded);
            return;
        }

        int elementsLeft = superSetElements.size() - position;

        if (elementsLeft < needToAdd) {
            // Hopeless branch - we have less elements left than we need to add.
            return;
        }

        Set<T> alreadyAddedWithElement = new HashSet<>(alreadyAdded);
        alreadyAddedWithElement.add(superSetElements.get(position));

        addSubSetsOfSizeK(superSetElements, position + 1, alreadyAdded, needToAdd, subSets);
        addSubSetsOfSizeK(superSetElements, position + 1, alreadyAddedWithElement, needToAdd - 1, subSets);
    }

    @Override
    public Set<Set<T>> allSubsetsOfSizeK(Set<T> set, int k) {
        if (k < 0 || k > set.size()) {
            throw new IllegalArgumentException("Subset size must be in range [0; set.size()]");
        }

        Set<Set<T>> result = new HashSet<>();
        addSubSetsOfSizeK(new ArrayList<T>(set), 0, new HashSet<T>(), k, result);

        return result;
    }
}
