package ee.ut.cs.aa.grading.lab_2_examples;

import ee.ut.cs.aa.grading.lab_2.combinatorics.BitVectorComparator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public final class Utils {
    private Utils() {
        // Utility class.
    }

    public static String bitVectorToString(boolean[] vector) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < vector.length; i++) {
            sb.append(vector[i] ? "1" : "0");
        }
        return sb.toString();
    }

    public static String bitVectorSetToStringSorted(Set<boolean[]> vectors) {
        List<boolean[]> vectorList = new ArrayList<>(vectors);
        vectorList.sort(new BitVectorComparator());

        StringBuilder sb = new StringBuilder("[");
        for (boolean[] vector : vectorList) {
            sb.append(bitVectorToString(vector));
            sb.append(", ");
        }
        sb.delete(sb.length() - 2, sb.length());
        sb.append("]");

        return sb.toString();
    }
}
