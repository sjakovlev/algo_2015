package ee.ut.cs.examples;

import ee.ut.cs.aa.grading.hash_table.HashFunction;

public class HashFunctionImpl implements HashFunction {

    private static double t = (Math.sqrt(5.0) - 1) / 2.0;

    /**
     * Hash function as defined in Task 1.
     *
     * This function returns an integer between 0 and m - 1 (inclusive).
     *
     * @param k item key
     * @param m size of a hash table
     * @return hash of the given key
     */
    @Override
    public int hash(int k, int m) {
        return (int)(m * (k * t - (int)(k * t)));
    }
}
