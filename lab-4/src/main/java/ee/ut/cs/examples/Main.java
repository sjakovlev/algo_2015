package ee.ut.cs.examples;

import ee.ut.cs.aa.grading.hash_table.HashTable;
import ee.ut.cs.aa.grading.hash_table.IntConverterFromInt;

public class Main {
    public static void main(String[] args) {
        //HashTable<Integer, String> hashTable = new HashBucketImpl<>();
        HashTable<Integer, String> hashTable = new HashOpenAddressingImpl<>();

        hashTable.setup(1, new HashFunctionImpl(), new IntConverterFromInt());

        hashTable.add(0, "zero");
        hashTable.add(1, "one");
        hashTable.add(2, "two");
        hashTable.add(3, "three");
        hashTable.add(4, "four");
        hashTable.add(5, "five");
        hashTable.add(6, "six");
        hashTable.add(7, "seven");
        hashTable.add(8, "eight");
        hashTable.add(9, "nine");

        System.out.println(hashTable.get(6));
        System.out.println(hashTable.get(7));
        System.out.println(hashTable.remove(7));
        System.out.println(hashTable.get(7));
        System.out.println(hashTable.get(1234));


    }
}
