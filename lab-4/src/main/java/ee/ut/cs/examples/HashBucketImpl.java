package ee.ut.cs.examples;

import ee.ut.cs.aa.grading.hash_table.HashFunction;
import ee.ut.cs.aa.grading.hash_table.HashTable;
import ee.ut.cs.aa.grading.hash_table.IntConverter;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class HashBucketImpl<K, V> implements HashTable<K, V> {

    private ArrayList<List<Element<K, V>>> hashTable;

    private HashFunction hashFunction;

    private IntConverter<K> intConverter;

    @Override
    public void setup(int size, HashFunction hashFunction, IntConverter<K> converter) {
        hashTable = new ArrayList<>(size);

        for (int i  = 0; i < size; i++) {
            hashTable.add(i, new LinkedList<Element<K, V>>());
        }

        this.hashFunction = hashFunction;
        this.intConverter = converter;
    }

    @Override
    public void add(K key, V value) throws IllegalStateException {
        if (hashTable == null || hashFunction == null || intConverter == null) {
            throw new RuntimeException("Cannot add element before setup");
        }

        int hash = hashFunction.hash(intConverter.convert(key), hashTable.size());

        hashTable.get(hash).add(new Element<K, V>(key, value));
    }

    @Override
    public V get(K key) {
        if (hashTable == null || hashFunction == null || intConverter == null) {
            throw new RuntimeException("Cannot add element before setup");
        }

        int hash = hashFunction.hash(intConverter.convert(key), hashTable.size());

        for (Element<K, V> element : hashTable.get(hash)) {
            if (element.getKey().equals(key)) {
                return element.getValue();
            }
        }

        return null;
    }

    @Override
    public V remove(K key) {
        if (hashTable == null || hashFunction == null || intConverter == null) {
            throw new RuntimeException("Cannot add element before setup");
        }

        int hash = hashFunction.hash(intConverter.convert(key), hashTable.size());

        for (Element<K, V> element : hashTable.get(hash)) {
            if (element.getKey().equals(key)) {
                hashTable.get(hash).remove(element);
                return element.getValue();
            }
        }

        return null;
    }
}
