package ee.ut.cs.examples;

import ee.ut.cs.aa.grading.hash_table.HashFunction;
import ee.ut.cs.aa.grading.hash_table.HashTable;
import ee.ut.cs.aa.grading.hash_table.IntConverter;

/**
 * Hast table implementation using open addressing.
 *
 * @param <K> type of keys
 * @param <V> type of values
 */
public class HashOpenAddressingImpl<K, V> implements HashTable<K, V> {

    private Element<K, V>[] elementArray;

    private HashFunction hashFunction;
    private IntConverter<K> intConverter;

    /**
     * Number of elements actually added to the hash table.
     */
    private int elementCount;

    @Override
    public void setup(int size, HashFunction hashFunction, IntConverter<K> converter) {

        // Check if size makes sense
        if (size <= 0) {
            throw new IllegalArgumentException("Size of a hash table must be at least 1");
        }

        //noinspection unchecked
        this.elementArray = new Element[size];
        this.hashFunction = hashFunction;
        this.intConverter = converter;
    }

    @Override
    public void add(K key, V value) throws IllegalStateException {
        checkInitialized();

        if (!canAddMore()) {
            // Not enough room - make array two times larger
            resize(elementArray.length * 2);
        }

        // Add element and increase number of elements
        addInternal(new Element<>(key, value));
    }

    @Override
    public V get(K key) {
        checkInitialized();

        // Find an element
        Integer position = getPositionByKey(key);

        // If found, return its value
        if (position != null) {
            return elementArray[position].getValue();
        }

        // Otherwise return null
        return null;
    }

    @Override
    public V remove(K key) {
        checkInitialized();

        // Find an element
        Integer position = getPositionByKey(key);

        // If found, remove it and return the value of the removed element
        if (position != null) {
            Element<K, V> removedElement = elementArray[position];
            removeInternal(position);
            return removedElement.getValue();
        }

        // Otherwise return null
        return null;
    }

    /**
     * Throw IllegalStateException if setup() method was not yet called.
     *
     * @throws IllegalStateException
     */
    private void checkInitialized() {
        if (elementArray == null) {
            throw new IllegalStateException("You must call setup() first!");
        }
    }

    /**
     * Get next position in open addressing chain.
     *
     * This implementation uses constant step of 1.
     *
     * @param currentPosition previous position
     * @return next position
     */
    private int getNextPosition(int currentPosition) {

        // Check if argument makes sense
        int size = elementArray.length;
        if (currentPosition < 0 || currentPosition >= size) {
            throw new IllegalArgumentException("Position out of bounds");
        }

        // Calculate next position
        int nextPosition = currentPosition + 1;

        // Wrap to the beginning if reached the end of the array
        while (nextPosition >= size) {
            nextPosition -= size;
        }

        return nextPosition;
    }

    /**
     * Check if another element can be added to the array without resizing it.
     *
     * Note that this method ensures that there will be at least one free spot left after the next element is added.
     *
     * @return true if at least one more element can be added without resizing the array
     */
    private boolean canAddMore() {
        return elementCount < elementArray.length - 1;
    }

    /**
     * Add element element array.
     *
     * @param element element that will be added
     */
    private void addInternal(Element<K, V> element) {

        // Ignore null elements
        if (element == null) {
            return;
        }

        // Calculate position (hash)
        int key = intConverter.convert(element.getKey());
        int size = elementArray.length;
        int position = hashFunction.hash(key, size);

        // Find a first free spot in the open addressing chain
        int checkedCount = 0;
        while (elementArray[position] != null) {

            if (checkedCount++ > size) {
                // Already checked the entire array - we are in an infinite loop
                // This will not happen if getNextPosition function is implemented correctly.
                throw new RuntimeException("Could not reach the end of open addressing chain");
            }

            position = getNextPosition(position);
        }

        // Place element to found position
        elementArray[position] = element;

        // Increase element count
        elementCount++;
    }

    /**
     * Remove an element from the given position and re-add all elements following the removed element
     * in open addressing chain.
     *
     * @param position position of an element that has to be removed
     */
    private void removeInternal(int position) {

        // Remove element at position
        elementArray[position] = null;

        // Decrease element count
        elementCount--;

        position++;
        Element<K,V> element;

        // Re-add all elements following the removed element in the open addressing chain
        while (elementArray[position] != null) {
            element = elementArray[position];
            elementArray[position] = null;
            addInternal(element);
        }
    }

    /**
     * Return position of an element with a given key.
     *
     * @param key key of an element that has to be found
     * @return position of found element or null if element with given key does not exist
     */
    private Integer getPositionByKey(K key) {

        // Calculate hash
        int intKey = intConverter.convert(key);
        int size = elementArray.length;
        int position = hashFunction.hash(intKey, size);

        // Find element or an end of open addressing chain
        int checkedCount = 0;
        while (elementArray[position] != null) {

            if (checkedCount++ > size) {
                // Already checked the entire array - we are in an infinite loop
                // This will not happen if getNextPosition function is implemented correctly.
                throw new RuntimeException("Could not reach the end of open addressing chain");
            }

            // If element found, return position.
            if (elementArray[position].getKey().equals(key)) {
                return position;
            }

            // If not, continue looking
            position = getNextPosition(position);
        }

        // Nothing found
        return null;
    }

    /**
     * Resize the internal element array.
     *
     * @param newSize size of the new array (must be greater than the number of elements in the table)
     */
    private void resize(int newSize) {

        // Check if new size makes sense
        if (newSize <= 0) {
            throw new IllegalArgumentException("Size of a hash table must be at least 1");
        }

        if (newSize <= elementCount) {
            throw new IllegalArgumentException("New size must be greater than the number of elements already added to" +
                    " the table");
        }

        // Hold a reference to old array
        Element<K, V>[] oldElementArray = this.elementArray;

        // Replace element array with a new one
        //noinspection unchecked
        this.elementArray = new Element[newSize];
        elementCount = 0;

        // Re-add elements from the old array
        for (Element<K, V> element : oldElementArray) {
            addInternal(element);
        }
    }
}
